// Supress normal form submission
$("#entry").submit(function(event) {
  event.preventDefault;
});
  
$("#attempt").keyup(function(event) {
  var txt = $("#attempt").val();  // Current content of the input field
  var keycode = event.which;      // The key that just went up
  var letter = String.fromCharCode(keycode);

  if (! "ABCDEFGHIJKLMNOPQRSTUVWXYZ".includes(letter)) {
    $("#attempt").val(txt.replace(/[^a-zA-Z]/, ""));
    txt = $('#attempt').val();
    highlight(txt);
  } else {

    $.getJSON("_checker", {text: txt}, function(data) {
      newMatch = data.result.newMatch;
      if (newMatch) {
        $("#resultshead").html("You found");
        $("#results").append(txt + "\n");
        $("#message").html("");
        $("#attempt").val("");
      } else if (data.result.msg) {
        $("#message").html(data.result.msg);
      } else if (data.result.del) {
        $("#attempt").val(txt.replace(/.$/, ""));
      }

      txt = $('#attempt').val();  // update txt before highlighting
      highlight(txt);

      if (data.result.url) {
        window.location.replace(data.result.url);
      }
      });
  }

  function highlight (txtv) {
    $(".word").each(function() {
      var re = new RegExp('^' + txtv);
      var repl = '<span class="highlight">' + txtv + '</span>';
      $(this).html($(this).html().replace(/<span class="highlight">|<\/span>/g, "").replace(re, repl));
    }) 

    var jmbl = $('#jumble').html().replace(/<span class="gray">|<\/span>/g, "").split('');   // Clean jumble
    var hglt = '';

    for (var i = 0; i < txtv.length; i++){ 
      for (var j = 0; j < jmbl.length; j++) {
        if (txtv[i] == jmbl[j]) {
          jmbl[j] = '<span class="gray">' + jmbl[j] + '</span>';
          break; // match found, prevents 'a' from highlighting 'aa'
        }
      }
    }
    for (var i = 0; i < jmbl.length; i++) {
      hglt += jmbl[i]; // rebuild jumble
    }
    $('#jumble').html(hglt);
  }
});